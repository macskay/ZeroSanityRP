HousePoints = HousePoints or {}

HOUSE_GRYFFINDOR = 1
HOUSE_RAVENCLAW = 2
HOUSE_HUFFLEPUFF = 3
HOUSE_SLYTHERIN = 4

local mainfolder = "housepoint/"
-- sh files
for k,v in pairs(file.Find(mainfolder .. "sh_*", "LUA")) do
    include(mainfolder .. tostring(v))
    if SERVER then AddCSLuaFile(mainfolder .. tostring(v)) end
end
-- sv files
if SERVER then
    for k,v in pairs(file.Find(mainfolder .. "sv_*", "LUA")) do
        include(mainfolder .. tostring(v))
    end
end
-- cl files
for k,v in pairs(file.Find(mainfolder .. "cl_*", "LUA")) do
    if SERVER then AddCSLuaFile(mainfolder ..  tostring(v))
    else include(mainfolder .. tostring(v))
    end
end


--- Credits to Star for this cool thing
HousePoints.allowedjobs = {}

hook.Add("Initialize", "HousePoints:JobLoader", function()
  for _, teamStr in ipairs(HousePoints.allowed) do
    local id = _G[teamStr]

    if id then
        HousePoints.allowedjobs[id] = true
    end
  end
end)