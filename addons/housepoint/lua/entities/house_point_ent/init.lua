AddCSLuaFile( "cl_init.lua" ) -- Make sure clientside
AddCSLuaFile( "shared.lua" )  -- and shared scripts are sent.
include('shared.lua')
function ENT:Initialize()
	self:SetModel( "models/glass_vial/glass_vial_frame.mdl" )
	self:PhysicsInit( SOLID_VPHYSICS )      -- Make us work with physics,
	self:SetMoveType( MOVETYPE_VPHYSICS )   -- after all, gmod is a physics
	self:SetSolid( SOLID_VPHYSICS )         -- Toolbox
	self:SetUseType(SIMPLE_USE)

	self.phys = self:GetPhysicsObject()
	self.phys:EnableMotion(false)
	if (self.phys:IsValid()) then
		self.phys:Wake()
	end
	
	self.slytherin = ents.Create( "prop_physics" )
	self.slytherin:SetModel("models/glass_vial/glass_vial.mdl")
	self.slytherin:SetPos( self:LocalToWorld(Vector(1.25, 0, 0)) )
	self.slytherin:SetColor(Color(0,255,0))
	self.slytherin:Spawn()
	self.slytherin:GetPhysicsObject():EnableMotion(false)
	self.slytherin:SetParent(self)
	self.slytherin:Activate()

	self.ravenclaw = ents.Create( "prop_physics" )
	self.ravenclaw:SetModel("models/glass_vial/glass_vial.mdl")
	self.ravenclaw:SetPos( self:LocalToWorld(Vector(1.25, -22, 0)) )
	self.ravenclaw:SetColor(Color(0,0,255))
	self.ravenclaw:Spawn()
	self.ravenclaw:GetPhysicsObject():EnableMotion(false)
	self.ravenclaw:SetParent(self)
	self.ravenclaw:Activate()

	self.gryffindor = ents.Create( "prop_physics" )
	self.gryffindor:SetModel("models/glass_vial/glass_vial.mdl")
	self.gryffindor:SetPos( self:LocalToWorld(Vector(1.25, -44, 0)) )
	self.gryffindor:SetColor(Color(255,0,0))
	self.gryffindor:Spawn()
	self.gryffindor:GetPhysicsObject():EnableMotion(false)
	self.gryffindor:SetParent(self)
	self.gryffindor:Activate()

	self.hufflepuff = ents.Create( "prop_physics" )
	self.hufflepuff:SetModel("models/glass_vial/glass_vial.mdl")
	self.hufflepuff:SetPos( self:LocalToWorld(Vector(1.25, -66, 0)) )
	self.hufflepuff:SetColor(Color(255,255,0))
	self.hufflepuff:Spawn()
	self.hufflepuff:GetPhysicsObject():EnableMotion(false)
	self.hufflepuff:SetParent(self)
	self.hufflepuff:Activate()
	
	self.slytherin:SetSequence(math.Round(HousePoints.Points[HOUSE_SLYTHERIN] / 10) + 2)
	self.ravenclaw:SetSequence(math.Round(HousePoints.Points[HOUSE_RAVENCLAW] / 10) + 2)
	self.gryffindor:SetSequence(math.Round(HousePoints.Points[HOUSE_GRYFFINDOR] / 10) + 2)
	self.hufflepuff:SetSequence(math.Round(HousePoints.Points[HOUSE_HUFFLEPUFF] / 10) + 2)
end

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end
	
	local SpawnPos = tr.HitPos + tr.HitNormal * 45 - ply:GetRight() * 30
	local SpawnAng = tr.HitNormal:Angle()
	SpawnAng.p = 0
	SpawnAng.y = SpawnAng.y + 180
	
	local ent = ents.Create( ClassName )
	ent:SetPos( SpawnPos )
	ent:SetAngles( SpawnAng )
	ent:Spawn()
	ent:Activate()
	
	return ent
	
end

function ENT:OnRemove()
	if IsValid(self.slytherin) then
	self.slytherin:Remove()
	end
	if IsValid(self.ravenclaw) then
		self.ravenclaw:Remove()
	end
	if IsValid(self.gryffindor) then
		self.gryffindor:Remove()
	end
	if IsValid(self.hufflepuff) then
		self.hufflepuff:Remove()
	end
end
