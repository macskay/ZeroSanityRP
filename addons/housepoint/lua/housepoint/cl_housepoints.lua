local frame
local background = Material("pointsystem/background.png")
local gryff = Material("pointsystem/gryffindor.png")
local slyth = Material("pointsystem/slytherin.png")
local huffle = Material("pointsystem/hufflepuff.png")
local ravenclaw = Material("pointsystem/ravenclaw.png")

local col1 = Color(0, 255, 60, 255)
local col2 = Color(255, 0, 60, 255)
surface.CreateFont( "PointSystem", {
	font = "Harry P",
	extended = false,
	size = 50,
} )

surface.CreateFont( "PointSystem_Header", {
	font = "Harry P",
	extended = false,
	size = 75,
} )

function HousePoints:PointMenu()
    if IsValid(frame) then frame:Remove() end
    frame = vgui.Create("DFrame")
    frame:SetSize(699, 694)
    frame:Center()
    frame:SetTitle("")
    frame:ShowCloseButton(false)
    frame:MakePopup()
    frame.Paint = function(s,w,h)
        surface.SetDrawColor(color_white)
        surface.SetMaterial(background)
        surface.DrawTexturedRect(0, 0, w, h)

        surface.SetMaterial(gryff)
        surface.DrawTexturedRect(75, 250, 150, 150)

        surface.SetMaterial(slyth)
        surface.DrawTexturedRect(75, 450, 150, 150)

        surface.SetMaterial(huffle)
        surface.DrawTexturedRect(375, 250, 150, 150)

        surface.SetMaterial(ravenclaw)
        surface.DrawTexturedRect(375, 450, 150, 150)
        draw.SimpleText("House-Points", "PointSystem_Header", 75, 125, color_black, TEXT_ALIGN_LEFT, TEXT_ALIGN_TOP)
    end
    ------------- gryff
    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(275,275)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("+10", "PointSystem", 30, 0, col1, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(1, 4)
        net.WriteInt(10, 5)
        net.SendToServer()
        frame:Remove()
    end

    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(275,340)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("-10", "PointSystem", 30, 0, col2, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(1, 4)
        net.WriteInt(-10, 5)
        net.SendToServer()
        frame:Remove()
    end

    ------------- huffle
    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(575,275)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("+10", "PointSystem", 30, 0, col1, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(3, 4)
        net.WriteInt(10, 5)
        net.SendToServer()
        frame:Remove()
    end

    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(575,340)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("-10", "PointSystem", 30, 0, col2, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(3, 4)
        net.WriteInt(-10, 5)
        net.SendToServer()
        frame:Remove()
    end

    ------------- slytherin
    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(275,475)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("+10", "PointSystem", 30, 0, col1, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(4, 4)
        net.WriteInt(10, 5)
        net.SendToServer()
        frame:Remove()
    end

    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(275,525)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("-10", "PointSystem", 30, 0, col2, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(4, 4)
        net.WriteInt(-10, 5)
        net.SendToServer()
        frame:Remove()
    end

    ------------- ravenclaw
    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(575,475)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("+10", "PointSystem", 30, 0, col1, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(2, 4)
        net.WriteInt(10, 5)
        net.SendToServer()
        frame:Remove()
    end

    local btn = vgui.Create("DButton", frame)
    btn:SetSize(60, 40)
    btn:SetPos(575,525)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("-10", "PointSystem", 30, 0, col2, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        net.Start("housepoints_points")
        net.WriteInt(2, 4)
        net.WriteInt(-10, 5)
        net.SendToServer()
        frame:Remove()
    end

    --- close
    local btn = vgui.Create("DButton", frame)
    btn:SetSize(50, 40)
    btn:SetPos(630,55)
    btn:SetText("")
    btn.Paint = function()
        draw.DrawText("X", "PointSystem", 25, 0, col2, TEXT_ALIGN_CENTER)
    end
    btn.DoClick = function()
        frame:Remove()
    end


end

hook.Add( "OnPlayerChat", "HousePoints:OpenMenu", function( ply, text) 
    if ply != LocalPlayer() then return end
    if string.lower(text) != HousePoints.menucmd then return end
    if not HousePoints.allowedjobs[ply:Team()] then return end
    HousePoints:PointMenu()
    return true
end )

HOUSE_GRYFFINDOR = 1
HOUSE_RAVENCLAW = 2
HOUSE_HUFFLEPUFF = 3
HOUSE_SLYTHERIN = 4
local houses = {
    [1] = "Gryffindor",
    [2] = "Ravenclaw",
    [3] = "Hufflepuff",
    [4] = "Slytherin",
}

net.Receive("housepoints_points", function()
    local house = net.ReadInt(4)
    local amount = net.ReadInt(5)
    local text = ""

    if amount > 0 then
        text = string.format(HousePoints.addmsg, houses[house], amount)
    else
        text = string.format(HousePoints.removemsg, houses[house], amount)
    end
    chat.AddText(HousePoints.clr, text)
end)
