-- Jobs that can assign points
HousePoints.allowed = {
    "TEAM_TEACHER", 
}
-- Command to open the menu
HousePoints.menucmd = "!hauspunkte"
-- Cooldown between giving points ( in seconds )
HousePoints.cooldown = 0
-- Adding message, dont change %s
HousePoints.addmsg = "[HousePoints] %s hat %s Punkte erhalten"
-- Removing message, dont change %s
HousePoints.removemsg = "[HousePoints] %s wuren %s Punkte abgezogen"
-- Color of the Chat Message
HousePoints.clr = Color(255, 131, 0, 255)
