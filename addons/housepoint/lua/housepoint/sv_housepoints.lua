HousePoints.Points = HousePoints.Points or {0, 0, 0, 0}
HousePoints.ents = HousePoints.ents or {}

local path1 = "housepoints/points.txt"
local path2 = "housepoints/ents.txt"
local enttable = {}

function HousePoints:LoadFiles()
    if not file.Exists("housepoints", "DATA")  then
        file.CreateDir("housepoints")
        file.Write(path1, "")
        file.Write(path2, "")
    else
        if not file.Exists(path1, "DATA") then
            file.Write(path1, "")
        end

        if not file.Exists(path2, "DATA") then
            file.Write(path2, "")
        end
    end

    local data = util.JSONToTable(file.Read(path1, "DATA"))
    if data != nil and istable(data) then 
        HousePoints.Points = data
    end

    local data = util.JSONToTable(file.Read(path2, "DATA"))
    if data != nil and istable(data) then 
        HousePoints.ents = data
    end

    PrintTable(HousePoints.ents)
    HousePoints.ents[game.GetMap()] = HousePoints.ents[game.GetMap()] or {}
    
    HousePoints:SpawnEnts()
end

function HousePoints:SaveFiles()
    file.Write(path1, util.TableToJSON(HousePoints.Points))
    file.Write(path2, util.TableToJSON(HousePoints.ents))
end

function HousePoints:AddPoints(house,amount)
    if not house or not amount then return end
    if not isnumber(amount) or not isnumber(house) then return end

    HousePoints.Points[house] = math.Clamp(HousePoints.Points[house] + amount,0,1000)

	local e = ents.FindByClass("house_point_ent")
	for i, ent in pairs(e) do
		HousePoints:AddEnt(ent)
	end
    for ent,_ in pairs(enttable) do
        if not IsValid(ent) then continue end
		ent.slytherin:SetSequence(math.Round(HousePoints.Points[HOUSE_SLYTHERIN] / 10) + 2)
        ent.ravenclaw:SetSequence(math.Round(HousePoints.Points[HOUSE_RAVENCLAW] / 10) + 2)
        ent.gryffindor:SetSequence(math.Round(HousePoints.Points[HOUSE_GRYFFINDOR] / 10) + 2)
        ent.hufflepuff:SetSequence(math.Round(HousePoints.Points[HOUSE_HUFFLEPUFF] / 10) + 2)
    end

    HousePoints:SaveFiles()
end

function HousePoints:AddEnt(ent) 
	print(ent)
    if not IsValid(ent) then return end
    if ent:GetClass() != "house_point_ent" and IsValid(ent:GetParent()) then ent = ent:GetParent() end
    if ent:GetClass() != "house_point_ent" then return end
    local data = {
        ["pos"] = ent:GetPos(), 
        ["ang"] = ent:GetAngles(),
    }
    local k = table.insert(HousePoints.ents[game.GetMap()], data)

    ent:Remove()

    local ent = ents.Create("house_point_ent")
    ent:SetPos(data["pos"])
    ent:SetAngles(data["ang"])
    ent.ident = k
    ent:Spawn()
    ent:GetPhysicsObject():EnableMotion(false)
    ent:GetPhysicsObject():Wake()
    enttable[ent] = true

    HousePoints:SaveFiles()
end

function HousePoints:RemoveEnt(ent) 
    if not IsValid(ent) then return end
    if ent:GetClass() != "house_point_ent" and IsValid(ent:GetParent()) then ent = ent:GetParent() print(ent) end
    if ent:GetClass() != "house_point_ent" then return end
    if not ent.ident then return end
    HousePoints.ents[game.GetMap()][ent.ident] = nil
    HousePoints:SaveFiles()
    ent:Remove()
end

function HousePoints:SpawnEnts()
    for k,data in pairs(HousePoints.ents[game.GetMap()]) do
        local ent = ents.Create("house_point_ent")
        ent:SetPos(data["pos"])
        ent:SetAngles(data["ang"])
        ent.ident = k
        ent:Spawn()
        ent:GetPhysicsObject():EnableMotion(false)
        ent:GetPhysicsObject():Wake()
        enttable[ent] = true
    end
end

hook.Add("InitPostEntity", "HousePoints:SpawnEnts", function() HousePoints:LoadFiles() end)

concommand.Add("housepoints_save", function(ply)
    if not ply:IsSuperAdmin() then return end
    local ent = ply:GetEyeTrace().Entity
    if enttable[ent] then return end
    HousePoints:AddEnt(ent)
end)

concommand.Add("housepoints_remove", function(ply)
    if not ply:IsSuperAdmin() then return end
    local ent = ply:GetEyeTrace().Entity
    HousePoints:RemoveEnt(ent) 
end)

concommand.Add("housepoints_resetpoints", function(ply)
    if not ply:IsSuperAdmin() then return end
    for i=1,4 do
        HousePoints.Points[i] = 0
    end

    for ent,_ in pairs(enttable) do
        if not IsValid(ent) then continue end
        ent.slytherin:SetSequence(math.Round(HousePoints.Points[HOUSE_SLYTHERIN] / 10) + 2)
        ent.ravenclaw:SetSequence(math.Round(HousePoints.Points[HOUSE_RAVENCLAW] / 10) + 2)
        ent.gryffindor:SetSequence(math.Round(HousePoints.Points[HOUSE_GRYFFINDOR] / 10) + 2)
        ent.hufflepuff:SetSequence(math.Round(HousePoints.Points[HOUSE_HUFFLEPUFF] / 10) + 2)
    end

    HousePoints:SaveFiles()
end)

util.AddNetworkString("housepoints_points")
local cooldown = 0
net.Receive("housepoints_points", function(_,ply)
    if not HousePoints.allowedjobs[ply:Team()] then return end
    if cooldown > CurTime() then ply:ChatPrint("House Points are on Cooldown!") return end

    local house = net.ReadInt(4)
    local amount = net.ReadInt(5)
    print(amount)
    if not house or not amount then return end
    if house < 1 or house > 4 then return end
    if amount != -10 and amount != 10 then return end

    HousePoints:AddPoints(house,amount)
    cooldown = CurTime() + HousePoints.cooldown

    net.Start("housepoints_points")
    net.WriteInt(house, 4)
    net.WriteInt(amount, 5)
    net.Broadcast() 
end)