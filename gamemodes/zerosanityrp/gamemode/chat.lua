include ("persistence.lua")

hook.Add("PlayerSay", "ProcessChatCommands", function(ply, text, bTeamChat) 
  if string.sub(text, 1, 1) == "!" then
    return ProcessChatCommand(ply, string.sub(text, 2))
  end
end)

local commands =
{
    ["rpname"] = SetRpName,
}

function ProcessChatCommand(ply, command)
  if string.sub(1, 7) == "rpname" then
    return SetRpName(ply, string.sub(7))
  end
end

function SetRpName(ply, newName)
  local oldName = ply:Nick()
  ChangeName(ply, newName)
  return newName .. " hat seinen Namen zu " .. ply:Nick() .. " geändert."
end

