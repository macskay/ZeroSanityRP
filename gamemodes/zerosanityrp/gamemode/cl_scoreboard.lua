scoreboard = scoreborad or {}
rpnames = rpnames or {}

net.Receive("RpNameChange", function(len, ply)
  local sid = net.ReadString()
  local rpname = net.ReadString()
  rpnames[sid] = rpname
end)

local PLAYER_LINE = {
  Init = function(self)
    self.Name = self:Add("DLabel")
    self.Name:Dock(LEFT)
    self.Name:SetFont("ScoreboardDefault")
    self.Name:SetTextColor(Color(255,255,255))
    self.Name:SetWidth(150)
    self.Name:DockMargin(4,0,0,0)

		self.Team = self:Add( "DLabel" )
		self.Team:Dock( FILL )
		self.Team:SetWidth( 50 )
		self.Team:SetFont( "ScoreboardDefault" )
    self.Team:SetTextColor(Color(255,255,255))
		self.Team:SetContentAlignment( 5 )

		self.Ping = self:Add( "DLabel" )
		self.Ping:Dock( RIGHT )
		self.Ping:SetWidth( 50 )
		self.Ping:SetFont( "ScoreboardDefault" )
    self.Ping:SetTextColor(Color(255,255,255))
		self.Ping:SetContentAlignment( 5 )

		self:Dock( TOP )
		self:DockPadding( 3, 3, 3, 3 )
		self:SetHeight( 32 + 3 * 2 )
		self:DockMargin( 2, 0, 2, 2 )
  end,

  Setup = function(self, pl)
    self.Player = pl
    self:Think(self)
  end,

  Think = function(self)
    if (!IsValid(self.Player)) then
      self:SetZPos(9999)
      self:Remove()
      return
    end

    local rpnameLookup = rpnames[self.Player:SteamID()]
    if  (rpnameLookup ~= nil && rpnameLookup != self.PName) then
        self.PName = rpnameLookup
        self.Name:SetText(self.PName)
    elseif (self.PName == nil || rpnameLookup == nil) then
        self.PName = self.Player:Nick()
        self.Name:SetText(self.PName)
    end

    if ( self.NumPing == nil || self.NumPing != self.Player:Ping() ) then
    	self.NumPing = self.Player:Ping()
    	self.Ping:SetText( self.NumPing )
    end
    
    if ( self.PTeam == nil || self.PTeam != self.Player:Team() ) then
    	self.PTeam = self.Player:Team()
    	self.Team:SetText( team.GetName(self.PTeam) )
    end

    if (self.Player:Team() == TEAM_CONNECTING) then
      self:SetZPos(2000 + self.Player:EntIndex())
    end
  end,

  Paint = function(self, w, h)
    if (!IsValid(self.Player)) then
      return
    end

    draw.RoundedBox(4, 0, 0, w, h, team.GetColor(self.Player:Team()))
  end
}

PLAYER_LINE = vgui.RegisterTable(PLAYER_LINE, "DPanel")

local SCORE_BOARD = {
  Init = function(self)
    self.Header = self:Add("Panel")
    self.Header:Dock(TOP)
    self.Header:SetHeight(100)

    self.Name = self.Header:Add("DLabel")
    self.Name:SetFont("ScoreboardDefaultTitle")
    self.Name:SetTextColor(color_white)
    self.Name:Dock(TOP)
    self.Name:SetHeight(40)
    self.Name:SetContentAlignment(5)
    self.Name:SetExpensiveShadow(2, Color(0,0,0,200))

    self.Students = self:Add("DScrollPanel")
    self.Students:Dock(FILL)
  end,
  PerformLayout = function(self)
    self:SetSize(700, ScrH() - 200)
    self:SetPos(ScrW() / 2 - 350, 100)
  end,
  Paint = function(self, w, h)
  end,
  Think = function(self, w, h)
    self.Name:SetText(GetHostName())
    local players = player.GetAll()
    for _, player in pairs(players) do
      if (IsValid(player.ScoreboardEntry)) then continue end

      player.ScoreboardEntry = vgui.CreateFromTable(PLAYER_LINE, player.ScoreboardEntry)
      player.ScoreboardEntry:Setup(player)
      self.Students:AddItem(player.ScoreboardEntry)
    end
  end
}

SCORE_BOARD = vgui.RegisterTable(SCORE_BOARD, "EditablePanel")

function scoreboard:show()
  if (!IsValid(gScoreboard)) then
    gScoreboard = vgui.CreateFromTable(SCORE_BOARD)
  end

  if (IsValid(gScoreboard)) then
    gScoreboard:Show()
    gScoreboard:MakePopup()
    gScoreboard:SetKeyboardInputEnabled(false)
  end
end

function scoreboard:hide()
  if (IsValid(gScoreboard)) then
    gScoreboard:Hide()
  end
end

function GM:ScoreboardShow()
  scoreboard:show()
end

function GM:ScoreboardHide()
  scoreboard:hide()
end
