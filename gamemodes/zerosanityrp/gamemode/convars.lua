util.AddNetworkString("RpNameChange")

concommand.Add( "createdb", function( ply, cmd, args )
  SetupPersistenceTable()
end )

concommand.Add( "dropdb", function( ply, cmd, args )
  DropPersistenceTable()
end )

concommand.Add( "persist", function( ply, cmd, args )
  SavePlayer(ply)
end )

concommand.Add( "set_team", function( ply, cmd, args )
  local team = args[1] or 1
  ChangeTeam(ply, team)
end )

concommand.Add( "give_weapon", function( ply, cmd, args )
  local wpn = args[1] or "n/a"
  ply:Give(wpn)
end )

concommand.Add( "printdb", function( ply, cmd, args )
  PrintTable(sql.Query("SELECT * FROM player_persistence"))
end )

concommand.Add( "set_rpname", function( ply, cmd, args )
  local newName = args[1] or 1
  ChangeName(ply, newName)
end )
