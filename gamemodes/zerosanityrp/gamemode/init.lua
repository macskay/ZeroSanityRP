AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )

AddCSLuaFile( "cl_chat.lua" )
AddCSLuaFile( "cl_scoreboard.lua")
AddCSLuaFile( "player_class/hogwarts_student.lua")

include("shared.lua" )
include("persistence.lua")
include("convars.lua")
include("chat.lua")

DEFINE_BASECLASS( "gamemode_sandbox" )

function GM:PlayerInitialSpawn(ply, transition)
  player_manager.SetPlayerClass( ply, "hogwarts_student" )
  ply:SetTeam(TEAM_STUDENT)

  local StoredPlayer = RetrieveAllColumns(ply)
  PrintTable(StoredPlayer)
  if (StoredPlayer ~= nil and StoredPlayer != false) then  
    ChangeName(ply, StoredPlayer[1]["rpname"])
    ply:SetTeam(StoredPlayer[1]["team"])
    local pos = Vector(StoredPlayer[1]["coord_x"], StoredPlayer[1]["coord_y"], StoredPlayer[1]["coord_z"])
    Msg(pos)
    ply:SetPos(Vector(0,0,0))
  end
end

function GetRpName(ply)
	rpname = RetrieveSingleColumn(ply, "rpname")
	if rpName ~= nil then
		return rpName
	else
		return ply:Nick()
	end
end

function ChangeName(ply, newName)
  PersistName(ply, newName)
  net.Start("RpNameChange")
    net.WriteString(ply:SteamID())
    net.WriteString(newName)
  net.Broadcast()
	Msg(ply:Nick() .. " heißt jetzt " .. newName)
	ply:Say(ply:Nick() .. " heißt jetzt " .. newName)
end

function ChangeTeam(ply, t)
	ply:SetTeam(t)
	ply:Spawn()
	Msg(GetRpName(ply) .. " ist jetzt ein/e " .. team.GetName(tonumber(t)))
	ply:Say(GetRpName(ply) .. " ist jetzt ein/e " .. team.GetName(tonumber(t)))
end
