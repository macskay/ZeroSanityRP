function RecreatePersistence()
  DropDatabase()
  SetupDatabase()
end

function DropPersistenceTable()
  query = "DROP TABLE player_persistence;"
  res = sql.Query(query)
  if res == false then
    Msg("Query: " .. query .. "\n")
    Msg("DropPersistenceTable: " .. sql.LastError(res) .. "\n")
  else
    Msg("Table successfully dropped")
  end
end

function SetupPersistenceTable()
   query = "CREATE TABLE player_persistence(unique_id varchar(255), coord_x real, coord_y real, coord_z real, team int, rpname varchar(255));"
   res = sql.Query(query)
   if res == false then
     Msg("Setup Failed: " .. sql.LastError(result) .. "\n")
   else
     Msg("Success! player_persistence created\n")
   end
end

function LoadPlayer(pl, transition)
   if !sql.TableExists("player_persistence") then
     SetupDatabase()
   end

       -- local steamid = MySafeString(pl:SteamID())
   if sql.TableExists("player_persistence") then
     res = RetrieveAllColumns(pl)
   end

   local pos = Vector(tonumber(res[1]["coord_x"]), tonumber(res[1]["coord_y"]), tonumber(res[1]["coord_z"]))
   pl:SetTeam(tonumber(res[1]["team"]), true, true)
   pl:SetPos(pos)
end
hook.Add("PlayerInitialSpawn", "LoadPlayer", LoadPlayer)

function SavePlayer(pl)
  local steamid = MySafeString(pl:SteamID())
  local pos = pl:GetPos()
  local x = pos.x
  local y = pos.y
  local z = pos.z
  local team = pl:Team()

  playerTable = {
    ["steamid"] = steamid,
    ["x"] = x,
    ["y"] = y,
    ["z"] = z,
    ["team"] = team,
    ["rpname"] = "unnamed"
  }

  if sql.TableExists("player_persistence") then
    rpname = RetrieveSingleColumn(pl, "rpname")
    if rpname ~= nil then
      playerTable["rpname"] = rpname
      PersistPlayer(playerTable, true)
    else
      playerTable["rpname"] = pl:Nick()
      PersistPlayer(playerTable, false)
    end
  else
      Msg("Table does not exist!\n")
  end
end

function PersistPlayer(tbl, bUpdate)
  if bUpdate then
    UpdatePlayerRecord(tbl)
  else
    InsertNewPlayerRecord(tbl)
  end
end

function InsertNewPlayerRecord(tbl)
  query = "INSERT INTO " ..
    "player_persistence (unique_id, coord_x, coord_y, coord_z, team, rpname) " ..
    "VALUES(" ..
    tbl["steamid"] .. ", " ..
    tbl["x"] .. ", " ..
    tbl["y"] .. ", " ..
    tbl["z"] .. ", " ..
    tbl["team"] .. ", " ..
    MySafeString(tbl["rpname"]) .. ")"
  res = sql.Query(query)
  if res == false then
    Msg("Query: " .. query .. "\n")
    Msg("InsertNewPlayerRecord: " .. sql.LastError(res) .. "\n")
  end
end

function UpdatePlayerRecord(tbl)
  query = "UPDATE player_persistence " ..
    "SET " ..
    "unique_id = " .. tbl["steamid"] .. ", " ..
    "coord_x = " .. tbl["x"] .. ", " ..
    "coord_y = " .. tbl["y"] .. ", " ..
    "coord_z = " .. tbl["z"] .. ", " ..
    "team = " .. tbl["team"] .. ", " ..
    "rpname = " .. MySafeString(tbl["rpname"]) .. ";"
  res = sql.Query(query)
  if res == false then
    Msg("Query: " .. query .. "\n")
    Msg("UpdatePlayerRecord: " .. sql.LastError(res) .. "\n")
  end
end

function RetrieveAllColumns(ply)
    local steamid = ply:SteamID()
    query = "SELECT * FROM player_persistence WHERE unique_id = " .. MySafeString(steamid) .. ";"
    return sql.Query(query)
end

function RetrieveSingleColumn(ply, column)
    local steamid = ply:SteamID()
    query = "SELECT " .. column .. " FROM player_persistence WHERE unique_id = " .. MySafeString(steamid) .. ";"
    res = sql.Query(query);
    if res ~= nil then
        if res == false then
          Msg("RetrieveSingleColumn: " .. sql.LastError(res) .. "\n")
        else
          return res[1]["rpname"]
        end
    end
end

function PersistName(ply, name)
  if sql.TableExists("player_persistence") then
    local steamid = ply:SteamID()
    query = "UPDATE player_persistence SET rpname = " .. MySafeString(name) .. " WHERE unique_id = " .. MySafeString(steamid) .. ";"
    print(query)
    res = sql.Query(query)
    if res == false then
      Msg("PersistName: " .. sql.LastError(res) .. "\n")
    end
  else
    Msg("Error: player_persistence does not exist")
  end
end

function MySafeString( str_in, bNoQuotes )
	local str = tostring( str_in )
	str = str:gsub( "'", "''" )

	local null_chr = string.find( str, "\0" )
	if ( null_chr ) then
		str = string.sub( str, 1, null_chr - 1 )
	end

	if ( bNoQuotes ) then
		return str
	end
	return "'" .. str .. "'"
end
