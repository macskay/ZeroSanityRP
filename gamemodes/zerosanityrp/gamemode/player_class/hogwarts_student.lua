DEFINE_BASECLASS( "player_default" )


local PLAYER = {}

PLAYER.WalkSpeed 	 = 200
PLAYER.RunSpeed		 = 400

function PLAYER:Loadout()
	self.Player:RemoveAllAmmo()
	self.Player:RemoveAllItems()
	self.Player:Give( "weapon_hpwr_stick" )
end

player_manager.RegisterClass("hogwarts_student", PLAYER, "player_default")
