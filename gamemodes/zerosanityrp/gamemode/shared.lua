DeriveGamemode("sandbox")
GM.Version = "1.0.0"
GM.Name = "ZeroSanityRP"
GM.Author = "Zero Sanity"

include("player_class/hogwarts_student.lua")

DEFINE_BASECLASS( "gamemode_base" )

-- SETUP TEAMS
TEAM_STUDENT = 1
team.SetUp(TEAM_STUDENT, "Neuer Schüler", Color(255, 255, 255, 20))

TEAM_GRYFFINDOR = 2
team.SetUp(TEAM_GRYFFINDOR, "Gryffindor", Color(175, 0, 1, 128))

TEAM_RAVENCLAW = 3
team.SetUp(TEAM_RAVENCLAW, "Ravenclaw", Color(34,47,91, 128))

TEAM_HUFFLEPUFF = 4
team.SetUp(TEAM_HUFFLEPUFF, "Hufflepuff", Color(240, 199, 94, 128))

TEAM_SLYTHERIN = 5
team.SetUp(TEAM_SLYTHERIN, "Slytherin", Color(42, 98, 61, 128))

TEAM_SLYTHERIN = 6
team.SetUp(TEAM_TEACHER, "Lehrkraft", Color(128, 128, 128, 128))
